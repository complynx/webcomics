# Transcript of Pepper&Carrot Episode 32 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 32: Slagmarken

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|True|Officer!
King|2|False|Fandt du en heks, som jeg beordrede?
Officer|3|True|Ja, min herre!
Officer|4|False|Hun står lige ved siden af Dem.
King|5|False|...?
Pepper|6|True|Hej!
Pepper|7|False|Mit navn er Pepp...
King|8|False|?!!
King|9|True|FJOLS!!!
King|10|True|Hvorfor kommer du med et barn?!
King|11|False|Jeg skal bruge en rigtig heks!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Undskyld mig!
Pepper|2|True|Jeg er en ægte Kaosah-heks.
Pepper|3|False|Jeg har endda et diplom som...
Writing|4|False|Kaosah
Writing|5|False|Diplom
Writing|6|False|Cayenne
Writing|7|False|Spidskommen
Writing|8|False|Timian
Writing|9|False|~ til Pepper -
King|10|False|STILLE!
King|11|True|Jeg har ikke brug for et barn i mit hær.
King|12|False|Gå hjem og leg med dine dukker.
Sound|13|False|Klask!
Army|14|True|HAHA HA HA!
Army|15|True|HAHA HA HA!
Army|16|True|HAHA HA HA!
Army|17|False|HAHA HAHA!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Jeg kan ikke tro det!
Pepper|2|False|Jeg har studeret i årevis, men ingen vil tage mig seriøst...
Pepper|3|False|... fordi jeg ikke ser erfaren nok ud!
Sound|4|False|PUF ! !
Pepper|5|False|CARROT !
Sound|6|False|BUM! !

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Seriøst, Carrot?
Pepper|2|True|Jeg håber maden var det værd.
Pepper|3|False|Du ser hæslig ud...
Pepper|4|False|...Du ser...
Pepper|5|True|Udseendet!
Pepper|6|False|Selvfølgelig!
Sound|7|False|Knæk!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|HEY!
Pepper|2|False|Jeg hørte I leder efter en RIGTIG HEKS ?!?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|True|Jeg har skiftet mening, tag barnet tilbage.
King|2|False|Det bliver nok for dyrt med hende dér.
Writing|3|False|FORTSÆTTES…

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Du kan også blive tilhænger og få dit navn skrevet her!
Pepper|2|False|Pepper & Carrot er fri, open-source og sponsoreret af sine læsere.
Pepper|3|False|Denne episode blev støttet af %%% tilhængere!
Pepper|4|False|Check www.peppercarrot.com for more info!
Pepper|5|False|Vi er på Patreon, Tipeee, PayPal, Liberapay … og andre!
Pepper|6|False|Tak!
Pepper|7|False|Vidste I det?
Credits|8|False|31 Marts, 2020 Tegning og manuskript: David Revoy. Genlæsning i beta-versionen: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Parnikkapore, Stephen Paul Weber, Valvin, Vejvej. Dansk Version Oversættelse: Emmiline Alapetite Rettelser: Rikke & Alexandre Alapetite Baseret på Hereva-universet Skabt af: David Revoy. Vedligeholdt af: Craig Maloney. Medforfattere: Craig Maloney, Nartance, Scribblemaniac, Valvin. Rettelser: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Værktøj: Krita 4.2.9-beta, Inkscape 0.92.3 on Kubuntu 19.10. Licens: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|9|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|10|False|You can also translate this page if you want.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
