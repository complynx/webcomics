# Transcript of Pepper&Carrot Episode 32 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 32 : Le champ de bataille

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|True|Officier !
King|2|False|Avez-vous engagé une sorcière, comme je l'ai demandé ?
Officer|3|True|Oui, mon roi !
Officer|4|False|Elle est juste là, à côté de vous.
King|5|False|...?
Pepper|6|True|Salut !
Pepper|7|False|Je m'appelle Pepp...
King|8|False|?!!
King|9|True|IMBÉCILE !!!
King|10|True|Pourquoi avez-vous engagé cette gamine ?!
King|11|False|J'ai besoin d'une vraie sorcière de combat !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Euh, pardon ?!
Pepper|2|True|Je suis une vraie Sorcière de Chaosah.
Pepper|3|False|J'ai même un diplôme qui...
Writing|4|False|Diplôme
Writing|5|False|de
Writing|6|False|Chaosah
Writing|7|False|Cayenne
Writing|8|False|Cumin
Writing|9|False|Thym
Writing|10|False|~ pour Pepper ~
King|11|False|SILENCE !
King|12|True|Je n'ai que faire d'une gamine dans cette armée.
King|13|False|Rentre chez toi jouer avec tes poupées.
Son|14|False|Slap !
Army|15|True|HAHA HA HA !
Army|16|True|HAHA HA HA !
Army|17|True|HAHA HA HA !
Army|18|False|HA HA HAHA !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|J'y crois pas !
Pepper|2|False|Toutes ces années d'études et personne ne me prend au sérieux sous prétexte que...
Pepper|3|False|...je n'ai pas assez d'expérience !
Son|4|False|POUF !!
Pepper|5|False|CARROT !
Son|6|False|PAF !!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Sérieusement, Carrot ?
Pepper|2|True|J'espère que ce repas en valait la peine.
Pepper|3|False|T'as l'air hideux...
Pepper|4|False|...T'as l'air...
Pepper|5|True|Les apparences !
Pepper|6|False|Bien sûr !
Son|7|False|Crac !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|HÉ !
Pepper|2|False|J'ai entendu dire que vous cherchiez une VRAIE SORCIÈRE ?!?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|True|Tout compte fait, rappelez la gamine.
King|2|False|Celle-ci doit coûter bien trop cher.
Writing|3|False|À SUIVRE...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Vous pouvez aussi devenir mécène de Pepper&Carrot et avoir votre nom inscrit ici !
Pepper|2|False|Pepper&Carrot est entièrement libre, gratuit, open-source et sponsorisé grâce au mécénat de ses lecteurs.
Pepper|3|False|Cet épisode a reçu le soutien de %%%% mécènes !
Pepper|4|False|Allez sur www.peppercarrot.com pour plus d'informations !
Pepper|5|False|Nous sommes sur Patreon, Tipeee, PayPal, Liberapay ... et d'autres !
Pepper|6|False|Merci !
Pepper|7|False|Le saviez-vous ?
Credits|8|False|Le 31 mars 2020 Art & scénario : David Revoy. Lecteurs de la version bêta : Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Parnikkapore, Stephen Paul Weber, Valvin, Vejvej. Version française originale Traduction : Nicolas Artance, David Revoy, Valvin, Quetzal2, Alkarex. Basé sur l'univers d'Hereva Créateur : David Revoy. Mainteneur principal : Craig Maloney. Rédacteurs : Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correcteurs : Willem Sonke, Moini, Hali, CGand, Alex Gryson . Logiciels : Krita 4.2.9-beta, Inkscape 0.92.3 on Kubuntu 19.10. Licence : Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|9|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|10|False|You can also translate this page if you want.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
