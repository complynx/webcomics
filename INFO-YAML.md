# info.yaml specification

This information is meant for people wishing to develop an API about the Pepper&Carrot episodes.
There are efforts to build a full API on top of this:

* https://www.peppercarrot.com/0_sources/episodes.json
* https://framagit.org/valvin/peppercarrot-api/
* https://framagit.org/Midgard/peppercarrot_api

---

Each episode directory has a `info.yaml` file that contains raw metadata about the episode. It may
contain the following keys. Unless otherwise stated below, any and all of those may be blank or
omitted, which means the information is not available.

## Character set
The files are encoded in UTF-8.

## Languages
All languages are expressed in *Pepper&Carrot pseudo-ISO-639 two letter language codes*. They are
defined in [langs.json](langs.json).

## Keys
### `id`
`string` This key will always be present and contain a unique, meaningful value that will never
change for published episodes. (It might change for unpublished episodes.) It is URL safe but might
not be particularly human readable. Human readable denotations of the episode should be extracted
from the episode SVGs.

### `serial`
`int≥0` This key will always be present and contain a unique value: the serial number in the
series. Values are consecutive across episodes (i.e. there are no gaps in the numbering), and
sorting on this field yields the intended order of the episodes. There is no explicit relationship
with `id`.

### `published`
`date` Publication date. If this key is omitted or blank, the episode is not yet published.

### `url`
`string` URL to the page on the official website where the episode is published. If `published` is
present, this key MUST be present and contain a valid URL.

### `original language`
`string` The language that the first version of the comic was originally written in.

### `financial supporters`
`{ string: int≥0|string }` Map of payment method to the amount of backers who supported this
episode with it. Some people might donate using several methods, but we don't track those. The
total amount of backers `n` is `min(ints) ≤ n ≤ sum(ints)`, but you may assume `n ≈ sum(ints)`.

The value in the map is an int ≥0 (exact number), a string of the form `~int` (approximate number)
or a string of the form `int1-int2` (approximately between given low and high; `int1` is always less
than `int2`). 

There's a special case: if present, the field `total` contains the total number of backers. It is
as least as precise as the other fields combined, and may be more precise.

The payment methods that are currently supported are:
* Patreon
* Liberapay
* PayPal
* Tipeee
* Ğ1
* bank transfer
* mail

But these may change at any time.

### `credits`
`{ string: {string: [string]|string} }` Map from scope (the language or special value `all`) to map from names to roles (free text). If the contributor is credited for multiple roles, the value is a list of them, otherwise it's just a string.

### `software`
`[ {string: string} ]` List of software used in episode production. The list entries are maps that
contain at least the key `name`, and may contain a `version`.

### `notes`
`[ {string: string} ]` List of official notes by episode contributors. The list entries are maps
that contain at least the key `author` (name of the note's author) and at least one language. The
language is the key, and the corresponding value is the body of the note in that language.

## Example
```yaml
---
id: "100"
serial: 102
published: 2030-04-13
url: https://www.peppercarrot.com/episodes/100/pepper-s-birthday-party-2

original language: jb

financial supporters:
  Patreon:         80135
  Liberapay:      203498
  bank transfer:     503

credits:
  all:
    David Revoy:     [art & scenario, universe creator]
    Hazel Sherman:   art & scenario
    Chasidy Naftali: [beta feedback, script doctor]
  en:
    Tessie Darkis: translator
    Alesha Sa:     proofreader
  fr:
    Maxime Denis:       [initial translator, corrector]
    Christian Bonnevie: reinterpreter
    Alphonse Reynaud:   corrector
    Nadége Duval:       proofreader
  nl:
    Tijs Van Belleghem: translator
    Guus Krijgsman:     translator

software:
  - name: Krita
    version: "7.0.2"
  - name: Inkscape
    version: "1.2.2"
  - name: Kubuntu
    version: "29.10"

notes:
  - author: David Revoy
    en: >
      Hi, today my big, big dream to reach 100 episodes comes true! A great thank you to everyone
      who made this possible by supporting my work over these past 16 years.
    fr: >
      Salut, aujourd'hui mon grand, grand rêve de parvenir à l'épisode 100 est arrivé ! Un gros
      merci à tous ceux qui ont aidé à financer mon travail pendant ces 16 dernières années.
    nl: >
      Hoi, vandaag komt mijn grote, grote droom uit van 100 afleveringen te maken! Een enorme
      dankjewel aan iedereen die dit mogelijk heeft gemaakt door mijn werk deze afgelopen 16 jaren
      te steunen.
```
