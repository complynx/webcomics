# Transcript of Pepper&Carrot Episode 20 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 20: The Picnic

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|- FIN -
Credits|2|False|12/2016 - www.peppercarrot.com - Art & Scenario : David Revoy
Credits|3|False|Based on the universe of Hereva created by David Revoy with contributions by Craig Maloney. Corrections by Willem Sonke, Moini, Hali, CGand and Alex Gryson.
Credits|4|False|License : Creative Commons Attribution 4.0, Software: Krita 3.1, Inkscape 0.91 on Manjaro XFCE

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers. For this episode, thanks go to 825 Patrons:
Credits|2|False|You too can become a patron of Pepper&Carrot at www.patreon.com/davidrevoy
