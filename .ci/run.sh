#!/bin/sh

curdir="$(dirname "$0")"

"$curdir"/metadata.py . >/dev/null 2> ci_errors; ci_ret=$?

cat ci_errors

if [ $ci_ret = 42 ];then
	# "$curdir"/comment.py < ci_errors; comment_ret=$?
	true
fi

# Check for file:/// links in SVG files
if "$curdir"/no_filelinks.sh; then
    # No file:/// links found, let's exit with the code from the other checks
	exit $comment_ret
else
	exit 1
fi

