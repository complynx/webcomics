# Transcript of Pepper&Carrot Episode 27 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 27: Innleadaireachd Chostag

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Neach-aithris|1|False|Àird Acainn air an fheasgar
Tàillear|4|False|A’ Bhana-phrionnsa Chostag...
Tàillear|5|False|Feuchaibh nach gluais sibh cus!
Costag|6|False|Feuchaidh, feuchaidh...
Tàillear|7|False|Tha an ùine a’ ruith oirnn ’s chan eil ach trì uairean a thìde air fhàgail mus tòisich ur crùnadh.
Costag|8|False|...
Sgrìobhte|2|True|CRÙNADH A MÒRACHD
Sgrìobhte|3|False|BÀNRIGH CHOSTAG
<hidden>|0|False|Translate “Her Majesty’s Coronation” and “Queen Coriander” on the banner on the building (in the middle). The other text boxes will update automatically (they are clones).
<hidden>|0|False|NOTE FOR TRANSLATORS
<hidden>|0|False|You can strech the scroll and translate “Qualicity” to “City of Qualicity”.
<hidden>|0|False|NOTE FOR TRANSLATORS

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Costag|1|True|Fhios agaibh...
Costag|2|False|Bha mi a’ smaoineachadh air an tèarainteachd a-rithist...
Costag|3|True|Chaidh grunn rìghrean ’s banrighrean a mharbhadh rè deas-ghnàth a’ chrùnaidh.
Costag|4|False|Seo pàirt de dh’eachdraidh mo theaghlaich.
Costag|5|True|Agus a-nis, an ceann beagan uairean a thìde, thig mìltean de dh’aoighean o fheadh an t-saoghail ’s bi mise aig cridhe an aire.
Peabar|7|True|Na gabh dragh, a Chostag.
Costag|6|False|Tha an t-eagal mòr mòr orm!
Peabar|8|True|Tha plana againn.
Peabar|9|False|Bidh Sidimi ’s mi fhìn ’nar lèintean-crios dhut mar a gheall sinn!
Sidimi|10|True|Sin dìreach
Sidimi|11|False|Air do shocair is smaoinich air rudeigin eile an-dràsta.
Costag|12|False|Ceart ma-thà.
Costag|13|False|Nise! Nach seall mi an innleadaireachd as ùire agam dhuibh?
Tàillear|14|False|...
Costag|15|True|Tha e shìos an staidhre sa cheàrdach agam.
Costag|16|False|Leanaibh orm.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Costag|1|False|Tuigidh sibh a dh’aithghearr carson a bu mhiann leam am fear ùr seo a thogail.
Costag|2|False|Tha e co-cheangailte ris an dragh agam mun chrùnadh.
Costag|3|False|Seo dhuibh an innleadaireachd as ùire agam...
Bot-leigheas-inntinn|5|False|Shin thu a shaoghail.
Bot-leigheas-inntinn|6|False|Is mise am BOT-LEIGHEAS-INNTINN agaibh blìob bliùb
Bot-leigheas-inntinn|7|False|Sìnibh air an langasaid agam airson sgrùdaidh an-asgaidh.
Fuaim|4|False|Briog!
Fuaim|8|False|Gnogag!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Costag|1|True|Nise, dè ur beachd?
Costag|2|False|Am bu toigh leibh fheuchainn?
Peabar|3|True|Bath.
Peabar|4|True|Chan eil feum air dhomhsa.
Peabar|5|False|Tha m’ inntinn slàn fallain
Fuaim|6|True|D|nowhitespace
Peabar|11|False|DÈ ’N DONAS A THA CEÀRR AIR AN ROBOTAIR AGAD?!
Costag|12|True|Duilich!
Costag|13|True|Tha buga aige nach deach leam fuasgladh fhathast.
Costag|14|False|Sadaidh e gath-dealain nuair a mhothaicheas e do bhreug.
Peabar|15|False|Breug?!
Peabar|16|False|Ach cha do dh’innis mi breug a-RIAMH ri m’ bheò !
Peabar|22|True|CHAN EIL...
Peabar|23|True|SEO...
Peabar|24|False|ÈIBHINN!!!
Fuaim|7|True|H|nowhitespace
Fuaim|8|True|H|nowhitespace
Fuaim|9|True|I|nowhitespace
Fuaim|10|False|!|nowhitespace
Fuaim|17|True|D
Fuaim|18|True|H|nowhitespace
Fuaim|19|True|H|nowhitespace
Fuaim|20|True|I|nowhitespace
Fuaim|21|False|!|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sidimi|1|False|Èibhinn? Chan eil idir! Duilich.
Costag|2|False|Hè hè! Tha sin fìor! Chan eil seo èibhinn air dòigh sam bith!
Peabar|8|True|Ha...
Peabar|9|True|Ha...
Peabar|10|False|Hà.
Costag|11|True|THOD!
Costag|12|False|Tha deise an deas-ghnàith millte! Bidh boile air an tàillear agam!
Costag|13|True|A dhroch-robotair!
Costag|14|False|Càirichidh mi am buga ort ’s gu buan!
Peabar|15|False|STAD!!!
Fuaim|3|True|D
Fuaim|4|True|H|nowhitespace
Fuaim|5|True|H|nowhitespace
Fuaim|6|True|I|nowhitespace
Fuaim|7|False|!|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|Tha beachd agam air dòigh sa chuireas sinn am buga seo gu feum.
Fuaim|2|False|Tiong!
Neach-aithris|3|False|Air an oidhche...
Costag|11|False|A Pheabar, nach tusa a tha glic!
Peabar|12|True|Ach chan eil idir.
Peabar|13|False|’S ann agadsa a bha an innleadaireachd!
Bot-leigheas-inntinn|14|False|An ath-neach.
Bot-leigheas-inntinn|15|True|Fàilte oirbh,
Bot-leigheas-inntinn|16|False|a bheil murt fainear dhuibh a-nochd?
Neach-aithris|17|False|Ri leantainn...
Fuaim|6|True|D
Fuaim|7|True|H|nowhitespace
Fuaim|8|True|H|nowhitespace
Fuaim|9|True|I|nowhitespace
Fuaim|10|False|!|nowhitespace
Sgrìobhte|4|True|CRÙNADH A MÒRACHD
Sgrìobhte|5|False|BÀNRIGH CHOSTAG

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|5|True|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd is chì thu d’ àinm an-seo!
Peabar|3|True|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean.
Peabar|4|False|Mòran taing dhan 1060 pàtran a thug taic dhan eapasod seo!
Peabar|7|True|Tadhail air www.peppercarrot.com airson barrachd fiosrachaidh!
Peabar|6|True|Tha sinn air Patreon, Tipeee, PayPal, Liberapay ...’s a bharrachd!
Peabar|8|False|Mòran taing!
Peabar|2|True|An robh fios agad?
Urram|1|False|31mh dhen Dàmhair 2018 Obair-ealain ⁊ sgeulachd: David Revoy. Leughadairean Beta: Amyspark, Butterfly, Bhoren, CalimeroTeknik, Craig Maloney, Gleki Arxokuna, Imsesaok, Jookia, Martin Disch, Midgard, Nicolas Artance, uncle Night, ValVin, xHire, Zveryok. Tionndadh Gàidhlig Eadar-theangachadh: GunChleoc . Stèidhichte air saoghal Hereva Air a chruthachadh le: David Revoy. Prìomh neach-glèidhidh: Craig Maloney. Sgrìobhadairean: Craig Maloney, Nartance, Scribblemaniac, Valvin. Ceartachadh: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Bathar-bog: Krita 4.2-dev, Inkscape 0.92.3 air Kubuntu 18.04. Ceadachas: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
