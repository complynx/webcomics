# Transcript of Pepper&Carrot Episode 01 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 1: The Potion of Flight

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|...and the last touch
Pepper|4|False|...mmm probably not strong enough
Sound|2|True|SHH
Sound|3|False|SHH
Sound|5|True|PLOP
Sound|6|False|PLOP

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|ha... perfect
Pepper|2|False|NO! Don't even think about it
Sound|3|False|S PL A S H|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Happy ?!
Credits|2|False|WWW.PEPPERCARROT.COM 05/2014
