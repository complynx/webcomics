# Transcript of Pepper&Carrot Episode 05 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 5: Eapasod sònraichte na Nollaige

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|1|False|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus 'ga sponsaireadh le taic phàtranan. Thug 170 pàtran taic dhan eapasod seo :
Urram|4|False|https://www.patreon.com/davidrevoy
Urram|3|False|Nach cuir thu taic dhan phroiseact? Bheir tabhartas dhe dholar air gach dealbh-èibhinn taic mhor dha Pepper&Carrot !
Urram|7|False|Innealan : Chaidh 100% dhen eapasod seo a dhèanamh le innealan saora aig a bheil bun-tùs fosgailte Krita, G'MIC, Blender air Ubuntu Gnome (GNU/Linux)
Urram|6|False|Tùs fosgailte : faidhlichean bun-tùis le breathan air càileachd àrd a ghabhas clò-bhualadh, le cruthan-clò ri a ghabhas luchdadh a-nuas, a reic, atharrachadh, eadar-theangachadh is msaa...
Urram|5|False|Ceadachas : Creative Commons Attribution dha "David Revoy" faodaidh tu obair eile bonntachadh air, atharrachadh, ath-phostadh, a reic is msaa...
Urram|2|False|Addison Lewis - Adrian Lord - Alan Hardman - Albert Westra - Alejandro Flores Prieto - Aleš Tomeček - Alexander Bülow Tomassen - Alexander Sopicki - Alex Lusco - Alex Silver - Alex Vandiver - Alfredo - Ali Poulton (Aunty Pol) - Allan Zieser Andreas Rieger - Andrej Kwadrin - Andrew - Andrew Godfrey - Andrey Alekseenko - Angela K - Anna Orlova - anonymous Antan Karmola - Ardash Crowfoot - Arne Brix - Aslak Kjølås-Sæverud - Axel Bordelon - Axel Philipsenburg - barbix - Ben Evans - Bernd - Boonsak Watanavisit - Boudewijn Rempt - Brett Smith - Brian Behnke - carlos levischi - Charlotte Lacombe-bar Chris Radcliff - Chris Sakkas - Christophe Carré - Christopher Bates - Clara Dexter - Colby Driedger - Conway Scott Smith Cyrille Largillier - Cyril Paciullo - Damien - Daniel - Daniel Björkman - david - Dmitry - Donald Hayward - Eitan Goldshtrom Enrico Billich--epsilon--Eric Schulz - Frederico - Garret Patterson - GreenAngel5 - Guillaume - Gustav Strömbom Guy Davis - Happy Mimic - Helmar Suschka - Ilyas Akhmedov - Inga Huang - Irene C. - Ivan Korotkov - James Frazier - Jared Tritsch - JDB - Jean-Baptiste Hebbrecht - Jeffrey Schneider - Jessey Wright - John - John - Jónatan Nilsson - Jonathan Leroy Jon Brake - Joseph Bowman - Jurgo van den Elzen - Justin Diehl - Kai-Ting (Danil) Ko - Kasper Hansen - Kathryn Wuerstl Ken Mingyuan Xia - Liselle - Lorentz Grip - MacCoy - Magnus Kronnäs - Mandy Streisel - marcus - Martin Owens Mary Brownlee - Masked Admirer - Mathias Stærk - mattscribbles - Maurice-Marie Stromer - mefflin ross bullis-bates Michael Gill - Michelle Pereira Garcia - Mike Mosher - Morten Hellesø Johansen - Muzyka Dmytro - Nazhif - Nicholas DeLateur Nick Allott - Nicki Aya - Nicola Angel - Nicolae Berbece - Nicole Heersema - Noble Hays - Nora Czaykowski - Nyx Olivier Amrein - Olivier Brun - Omar Willey - Oscar Moreno - Öykü Su Gürler - Ozone S. - Paul - Paul - Pavel Semenov Peter - Peter Moonen - Petr Vlašic - Pierre Geier - Pierre Vuillemin - Pranab Shenoy - Rajul Gupta - Ret Samys - rictic RJ van der Weide - Roberto Zaghis - Roman - Rumiko Hoshino - Rustin Simons - Sally Bridgewater - Sami T - Samuel Mitson Scott Russ - Sean Adams Shadefalcon - Shane Lopez - Shawn Meyer - Sigmundur Þórir Jónsson - Simon Forster Simon Isenberg - Sonny W. - Stanislav Vodetskyi - Stephen Bates - Steven Bennett - Stuart Dickson - surt - Tadamichi Ohkubo tar8156 - TheFaico - Thomas Schwery - Tracey Reuben - Travis Humble - tree - Vespertinus - Victoria - Vlad Tomash Westen Curry - Xavier Claude - Yalyn Vinkindo - Алексей - Глеб Бузало - 獨孤欣 & 獨弧悦
